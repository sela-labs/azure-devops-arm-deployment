# **Deploy ARM template using pipeline as code (yaml)**

**1.** Create new pipeline and choose **Azure Repos Git.**

**2.** Select the desired repository

**3.** For   **Configure your pipeline**  choose **any template** ( we will override it later)

**4.** Paste the following yml into the code block –

```shell
pool:
  vmImage: 'ubuntu-latest'

steps:
- task: AzureResourceGroupDeployment@2
  displayName: 'Azure Deployment:Create Or Update Resource Group action on Resource group'
  inputs:
    azureSubscription: '%AZURE_SUBSCRIPTION_NAME% (%AZURE_SUBSCRIPTION_ID%)'
    resourceGroupName: '%AZURE_RESOURCE_GROUP_NAME%'
    location: '%AZ_LOCATION%'
    templateLocation: 'URL of the file'
    csmFileLink: 'https://raw.githubusercontent.com/Azure/azure-quickstart-templates/master/101-webapp-basic-linux/azuredeploy.json'
    overrideParameters: '-webAppName $(webAppName)'
```


**5.** Update the yml with relevant data. ( Replace all %Variable% with your data)

**6.** Click on **variables** at the top right corner.

**7.** Click on ```(+)``` And create new variable called 
```webAppName``` 
The value will be the name of the App Service plan we about to create. You can choose any name you would like.

**8.** Click on **OK**.

**9.** Save **the template and click on** ```Run```.

**10.** Be aware that when running the job you might asked to allow the permission to run azure commands from this job against your environment. In that case, just click on allow.

**11.** After the deployment complted go to your resoue group, Now you should see new App Service Plan called – ```AppServicePlan%webAppName%```